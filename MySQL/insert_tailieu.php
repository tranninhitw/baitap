<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>THÊM TÀI LIỆU</title>
    <style type="text/css">
            form
            {
                background-color: #ccd9cf;

            }
            h2
            {
                background-color: #2d9498;
                text-align: center;
            }
            #searchBtn
            {
                background-color: #f9f895;
            }    
        </style>    
</head>
<body>
    <?php 
        require('connect.php');
    ?>
    <form action="" method="post" enctype="multipart/form-data">
        <table align="center" width="70%" border="1">
            <tr>
                <td align="center" colspan="2" style="background-color: #2d9498;"><h2>THÊM TÀI LIỆU</h2></td>
            </tr>
            <tr>
                <td align="center" width="50%">Chọn tên thể loại: <select name="the_loai">
                    <?php
                        $sql = "SELECT * FROM theloai";
                        $result = mysqli_query($conn, $sql);
                        if(mysqli_num_rows($result) > 0)
                        {
                           while($row = mysqli_fetch_array($result))
                           {
                               $ma_loai = $row['MALOAI'];
                               $ten_loai = $row['TENLOAI'];
                               echo '<option value="'.$ma_loai.'"';
                               if(isset($_REQUEST['the_loai']) && ($_REQUEST['the_loai']==$ma_loai))
                               {
                                    echo 'selected="selected"';
                               } 
                               echo ">".$ten_loai."</option>";
                           } 
                        }
                        mysqli_free_result($result);
                    ?>
                    </select>
                          
                </td>
                <td align="center">Chọn tên tác giả: <select name="tac_gia">
                    <?php
                        $sql = "SELECT * FROM tacgia";
                        $result = mysqli_query($conn, $sql);
                        if(mysqli_num_rows($result) > 0)
                        {
                           while($row = mysqli_fetch_array($result))
                           {
                               $ma_tg = $row['MATG'];
                               $ten_tg = $row['TENTG'];
                               echo '<option value="'.$ma_tg.'"';
                               if(isset($_REQUEST['tac_gia']) && ($_REQUEST['tac_gia']==$ma_tg))
                               {
                                    echo 'selected="selected"';
                               } 
                               echo ">".$ten_tg."</option>";
                           } 
                        }
                        mysqli_free_result($result);
                    ?>
                    </select>
                      
                </td>
            </tr>
            <tr>
                <td align="center">Mã tài liệu: 
                    <input type="text"  <?php   
                        $count = 'SELECT * FROM `tailieu` WHERE 1';
                        $count_result =  mysqli_query($conn, $count);
                        $num = 1;
                        echo 'value="S00'.(mysqli_num_rows($count_result)+1).'"';
                     ?> name="ma_tai_lieu" 
                     />
                    
                </td>
                <td align="center">
                    Tên tài liệu:
                    <input type="text" value="" name="ten_tai_lieu"/>
                </td>
            </tr>
            <tr>
                <td align="center">Số trang: 
                    <input type="text" value="" name="so_trang"/>
                    
                </td>
                <td align="center">
                    Năm phát hành:
                    <input type="text" value="" name="nam_phat_hanh"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">Ảnh: 
                    <input type="file" id='file' name="anh"/>
                    
                </td>
                
            </tr>
            <tr>
                <td colspan="2" align="center"> 
                    <input type="submit" value="Thêm" name="searchBtn" id="searchBtn">
                    
                </td>
                
            </tr>
        </table>
        <?php 
            if(isset($_POST["searchBtn"]))
            {
                $ma_loai = $_REQUEST["the_loai"];
                $ma_tg = $_REQUEST["tac_gia"];
                $ma_tai_lieu = $_REQUEST["ma_tai_lieu"];
                $ten_tai_lieu = $_REQUEST["ten_tai_lieu"];
                $so_trang = $_REQUEST["so_trang"];
                $nam_phat_hanh = $_REQUEST["nam_phat_hanh"];
                $anh = $_FILES["anh"]["name"];
                if ($ma_loai != null && $ma_tg != null && $ma_tai_lieu != null && $ten_tai_lieu != null && $so_trang != null && $nam_phat_hanh != null && $anh != null)
                {   $sql_insert = "INSERT INTO `tailieu` (`MATL`, `TENTL`, `ANH`, `SOTRANG`, `NAMPH`, `MALOAI`, `MATG`) VALUES ('".$ma_tai_lieu."', '".   $ten_tai_lieu."', '".$anh."', '".$so_trang."', '".$nam_phat_hanh."', '".$ma_loai."', '".$ma_tg."');";
                
                      if(mysqli_query($conn, $sql_insert) && $ma_loai != null)
                    {   
                      
                        
                       echo '<p align="center">Đã thêm tài liệu thành công!</p>';
                    }
                      else
                      {
                      
                      echo '<p align="center">Thêm tài liệu không thành công!</p>';
                      }
                }
                else
                {
                  echo '<p align="center">Xin vui lòng nhập đầy đủ thông tin!</p>';
                }
              $conn->close();
            }
            else {
               $ma_loai = null;
                $ma_tg = null;
                $ma_tai_lieu = null;
                $ten_tai_lieu = null;
                $so_trang = null;
                $nam_phat_hanh = null;
                $anh = null;
            }
           
        ?>
        
    </form>
</body>
</html>