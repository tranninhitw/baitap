-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 02, 2019 lúc 08:17 AM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `nguyentuandat`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tacgia`
--

CREATE TABLE `tacgia` (
  `MATG` varchar(10) NOT NULL,
  `TENTG` varchar(100) CHARACTER SET utf8 NOT NULL,
  `QUOCTICH` varchar(50) CHARACTER SET utf8 NOT NULL,
  `EMAIL` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `tacgia`
--

INSERT INTO `tacgia` (`MATG`, `TENTG`, `QUOCTICH`, `EMAIL`) VALUES
('K001', 'Khoa Công Nghệ', 'Việt Nam', 'kcntt@gmail.com'),
('NV001', 'Tố Hữu', 'Việt Nam', 'tohuu@gmail.com'),
('NV002', 'Kim Lân', 'Việt Nam', 'kimlan@gmail.com'),
('NV003', 'Ngô Tất Tố', 'Việt Nam', 'ngotatto@gmail.com'),
('NV004', 'Xét-Van-Tet', 'Tây Ban Nha', 'xetvante@gmail.com'),
('NV005', 'Kim Dung', 'Trung Hoa', 'kimdung@gmail.com');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tailieu`
--

CREATE TABLE `tailieu` (
  `MATL` varchar(10) NOT NULL,
  `TENTL` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ANH` varchar(100) NOT NULL,
  `SOTRANG` varchar(5) NOT NULL,
  `NAMPH` varchar(5) NOT NULL,
  `MALOAI` varchar(10) NOT NULL,
  `MATG` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `tailieu`
--

INSERT INTO `tailieu` (`MATL`, `TENTL`, `ANH`, `SOTRANG`, `NAMPH`, `MALOAI`, `MATG`) VALUES
('S001', 'Từ Ấy', 'Tu_Ay.jpg', '100', '1937', 'TL004', 'NV001'),
('S002', 'Vợ nhặt', 'Vo_nhat.jpg', '100', '1962', 'TL003', 'NV002'),
('S003', 'Cối xây gió', 'coi_xay_gio.jpg', '200', '1950', 'TL002', 'NV004'),
('S004', 'Ngô Việt Xuân Thu', 'Ngo_Viet.jpg', '200', '1953', 'TL001', 'NV003'),
('S005', 'Uy Thiên Đồ Long Ký', 'Long_Ky.jpg', '500', '1961', 'TL002', 'NV005'),
('S006', 'Ky thuật đồ họa', 'ktdh.jpg', '100', '2010', 'TL005', 'K001'),
('S007', 'Gió Lộng', 'gio_long.jpg', '100', '1961', 'TL004', 'NV001'),
('S008', 'Tắt đèn', 'tat_den.jpg', '55', '1937', 'TL001', 'NV003');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `theloai`
--

CREATE TABLE `theloai` (
  `MALOAI` varchar(10) NOT NULL,
  `TENLOAI` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `theloai`
--

INSERT INTO `theloai` (`MALOAI`, `TENLOAI`) VALUES
('TL001', 'Văn Xuôi'),
('TL002', 'Văn học nước ngoài'),
('TL003', 'Truyện Ngắn'),
('TL004', 'Thơ'),
('TL005', 'Công nghệ');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tacgia`
--
ALTER TABLE `tacgia`
  ADD PRIMARY KEY (`MATG`);

--
-- Chỉ mục cho bảng `tailieu`
--
ALTER TABLE `tailieu`
  ADD PRIMARY KEY (`MATL`),
  ADD KEY `FK_tailieu_theloai` (`MALOAI`),
  ADD KEY `FK_tailieu_tacgia` (`MATG`);

--
-- Chỉ mục cho bảng `theloai`
--
ALTER TABLE `theloai`
  ADD PRIMARY KEY (`MALOAI`);

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `tailieu`
--
ALTER TABLE `tailieu`
  ADD CONSTRAINT `FK_tailieu_tacgia` FOREIGN KEY (`MATG`) REFERENCES `tacgia` (`MATG`),
  ADD CONSTRAINT `FK_tailieu_theloai` FOREIGN KEY (`MALOAI`) REFERENCES `theloai` (`MALOAI`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
