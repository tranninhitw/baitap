<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tìm kiếm nâng cao</title>
    <style type="text/css">
            form
            {
                background-color: #ccd9cf;

            }
            h2
            {
                background-color: #2d9498;
                text-align: center;
            }
            #searchBtn
            {
                background-color: #f9f895;
            }    
        </style>    
</head>
<body>
    <?php 
        require('connect.php');
    ?>
    <form action="" method="post">
        <table align="center" width="70%" border="1">
            <tr>
                <td align="center" colspan="2" style="background-color: #2d9498;"><h2>TÌM KIẾM THÔNG TIN TÀI LIỆU</h2></td>
            </tr>
            <tr width="70%">
                <td align="center">Thể loại: <select name="the_loai">
                    <?php
                        $sql = "SELECT * FROM theloai";
                        $result = mysqli_query($conn, $sql);
                        if(mysqli_num_rows($result) > 0)
                        {
                           while($row = mysqli_fetch_array($result))
                           {
                               $ma_loai = $row['MALOAI'];
                               $ten_loai = $row['TENLOAI'];
                               echo '<option value="'.$ma_loai.'"';
                               if(isset($_REQUEST['the_loai']) && ($_REQUEST['the_loai']==$ma_loai))
                               {
                                    echo 'selected="selected"';
                               } 
                               echo ">".$ten_loai."</option>";
                           } 
                        }
                        mysqli_free_result($result);
                    ?>
                    </select>
                    <input type="submit" value="Tìm kiếm" name="searchBtn" id="searchBtn">      
                </td>
            </tr>
            
        </table>
        <?php 
            if(isset($_POST["searchBtn"]))
            {
                $ma_loai = $_REQUEST["the_loai"];
                $sql_search = "SELECT MATL, TENTL, SOTRANG, NAMPH, TENTG, ANH, TENLOAI
                            FROM tailieu JOIN theloai ON tailieu.MALOAI = theloai.MALOAI
                            JOIN tacgia ON tailieu.MATG = tacgia.MATG
                            WHERE tailieu.MALOAI LIKE '".$ma_loai."'";
                $sql_tenloai = "SELECT TENLOAI FROM theloai WHERE MALOAI LIKE '".$ma_loai."'";
                $res_search = mysqli_query($conn, $sql_search);
                $result = mysqli_query($conn, $sql_tenloai);
                if(mysqli_num_rows($res_search) > 0 )
                {
                    
                    echo '<p align="center">Có '.mysqli_num_rows($res_search).' sản phẩm được tìm thấy</p>';
                    echo '<p align="center">Danh mục tài liệu theo "'.mysqli_fetch_object($result)->TENLOAI.'"</p>';
                    echo '<table border="1" width="70%" align="center">';
                    echo '<tr>';
                            echo '<td align="center"><b>Stt</b></td>';
                            echo '<td colspan="2" align="center" style="background-color: #2d9498;">THÔNG TIN TÀI LIỆU';
                    echo '</tr>';
                    $stt = 1;
                    while($row = mysqli_fetch_object($res_search))
                    {
                        
                        echo '<tr>';
                            echo '<td align="center"><b>'.$stt.'</b></td>';
                            echo '<td align="center" width="200px"> <img src="Hinh_tailieu/'.$row->ANH.'" width="100px" height="100px"></td>';
                        
                            echo '<td>';                  
                                echo '<b> Mã số: </b>'.$row->MATL.'</br>';
                                echo '<b>Tên tài liệu:</b>'.$row->TENTL.'</br>';
                                echo '<b>Số trang: </b>'.$row->SOTRANG.'</br>';
                                echo '<b>Năm phát hành:</b>'.$row->NAMPH.'</br>';
                                echo '<b>Tên tác giả: </b>'.$row->TENTG;
                            echo '</td>';

                        echo '</tr>'; 
                         $stt++;                      
                    }
                    
                    echo '</table>';
                }
                else
                {
                    echo '<p align="center">Không tìm thấy danh mục sản phẩm "'.mysqli_fetch_object($result)->TENLOAI.'"!</p>';
                }
            }
            else 
            {
                echo '<p align="center">Vui lòng nhập thông tin sản phẩm cần tìm!</p>';
            }
        ?>
    </form>
</body>
</html>