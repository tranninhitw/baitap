<!DOCTYPE html>
<html lang="vi">
<head>
	 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Việt Minh - Tuấn Đạt</title>
	<style>
		table
        {
            background-color: #ccd9cf;

        }
        #headerTable
        {
            background-color: #2d9498;
            text-align: center;
        }
	</style>
</head>
<body>
	<?php
		require('VietMinh_TuanDat_connect.php');
		$sql = "SELECT ma_tv, ten_tv, mat_ma FROM thanhvien";
    	$result = mysqli_query($conn, $sql);
    	function printDS($result)
    	{
    		if(mysqli_num_rows($result)!=0)
    		{
    			$dem = 0;
        		while ($row = mysqli_fetch_object($result))
        		{   
        			if($dem == 1)
        			{
        				$str = 'style= "background-color: lightblue;"';
        				$dem = 0;
        			}
        			else
        			{
        				$str = 'style= "background-color: lightpink;"';
        				$dem = 1;
        			}
        			echo '<tr '.$str.'>';
        			
        			foreach ($row as $key => $value) 
        			{
                        if($key == "ma_tv")
                        {
                            echo '<td align="center"><a href="VietMinh_TuanDat_chitiet.php?ma_tv='.$row->ma_tv.'">'.$value.'</a></td>';
                        } else  
                            echo '<td align="center">'.$value.'</td>';
                        
        				    
                    }
                    
        			echo '</tr>';
        		}
    		}
    	}
	?>
    <form action="" method="GET">
	<table border="1" align="center">
			<tr id="headerTable">
				<td colspan="8"><h3 align="center">THÔNG TIN THÀNH VIÊN</h3></td>
			</tr>
			<tr>
				<th>Mã thành viên</th>
				<th>Tên thành viên</th>
				<th>Mật mã</th>
			</tr>
			<?php printDS($result); ?>
			
	</table>
    </form>
</body>
</html>