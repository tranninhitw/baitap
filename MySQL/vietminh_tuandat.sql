-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2019 at 01:39 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vietminh_tuandat`
--

-- --------------------------------------------------------

--
-- Table structure for table `baiviet`
--

CREATE TABLE `baiviet` (
  `ma_bv` varchar(10) NOT NULL,
  `tieu_de` varchar(20) NOT NULL,
  `noi_dung` varchar(50) NOT NULL,
  `ma_cd` varchar(10) NOT NULL,
  `ma_tv` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `baiviet`
--

INSERT INTO `baiviet` (`ma_bv`, `tieu_de`, `noi_dung`, `ma_cd`, `ma_tv`) VALUES
('bv01', 'Công nghệ phần mềm', 'sách giáo khoa-công nghệ', 'cd01', 'tv01'),
('bv02', 'Công nghệ phần cứng', 'sách khoa học', 'cd02', 'tv02'),
('bv03', 'tiêu đề 03', 'nội dung 03', 'cd03', 'tv03'),
('bv04', 'tiêu đề 04', 'nội dung 04', 'cd04', 'tv04');

-- --------------------------------------------------------

--
-- Table structure for table `chude`
--

CREATE TABLE `chude` (
  `ma_cd` varchar(10) NOT NULL,
  `ten_cd` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chude`
--

INSERT INTO `chude` (`ma_cd`, `ten_cd`) VALUES
('cd01', 'tên chủ đề 1'),
('cd02', 'tên chủ đề 2'),
('cd03', 'tên chủ đề 3'),
('cd04', 'tên chủ đề 4');

-- --------------------------------------------------------

--
-- Table structure for table `thanhvien`
--

CREATE TABLE `thanhvien` (
  `ma_tv` varchar(10) NOT NULL,
  `ten_tv` varchar(50) NOT NULL,
  `mat_ma` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `thanhvien`
--

INSERT INTO `thanhvien` (`ma_tv`, `ten_tv`, `mat_ma`) VALUES
('tv01', 'Nguyễn Tuấn Đạt', 'abc123'),
('tv02', 'Trương Việt Minh', 'abc456'),
('tv03', 'Nguyễn Văn Giáp', 'abc789'),
('tv04', 'Nguyễn Văn Ất', 'def123');

-- --------------------------------------------------------

--
-- Table structure for table `thaoluan`
--

CREATE TABLE `thaoluan` (
  `ma_tv` varchar(10) NOT NULL,
  `ma_bv` varchar(10) NOT NULL,
  `noi_dung_tl` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `thaoluan`
--

INSERT INTO `thaoluan` (`ma_tv`, `ma_bv`, `noi_dung_tl`) VALUES
('tv01', 'bv01', 'thảo luận khoa học'),
('tv02', 'bv02', 'thảo luận cho vui'),
('tv03', 'bv03', 'thảo luận chi tiết'),
('tv04', 'bv04', 'thảo luận tương quan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `baiviet`
--
ALTER TABLE `baiviet`
  ADD PRIMARY KEY (`ma_bv`),
  ADD KEY `fk_bv_tv` (`ma_tv`),
  ADD KEY `fk_bv_cd` (`ma_cd`);

--
-- Indexes for table `chude`
--
ALTER TABLE `chude`
  ADD PRIMARY KEY (`ma_cd`);

--
-- Indexes for table `thanhvien`
--
ALTER TABLE `thanhvien`
  ADD PRIMARY KEY (`ma_tv`);

--
-- Indexes for table `thaoluan`
--
ALTER TABLE `thaoluan`
  ADD PRIMARY KEY (`ma_tv`,`ma_bv`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `baiviet`
--
ALTER TABLE `baiviet`
  ADD CONSTRAINT `fk_bv_cd` FOREIGN KEY (`ma_cd`) REFERENCES `chude` (`ma_cd`),
  ADD CONSTRAINT `fk_bv_tv` FOREIGN KEY (`ma_tv`) REFERENCES `thanhvien` (`ma_tv`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
