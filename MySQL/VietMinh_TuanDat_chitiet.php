<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cập nhật thông tin khách hàng</title>
    <style>
        table
        {
            background-color: #ccd9cf;

        }
        th
        {
            background-color: #2d9498;
            text-align: center;
        }
        #updateBtn
        {
                background-color: #f9f895;
        }
    </style>
</head>
<body>
    <?php
        require('VietMinh_TuanDat_connect.php');
        if(isset($_GET["ma_tv"]))
        {
            $ma_tv=$_GET['ma_tv']; 
        }
        else
            $ma_tv = "";

        $query = "SELECT * 
                  FROM baiviet JOIN thanhvien ON thanhvien.ma_tv = baiviet.ma_tv
                                JOIN chude ON baiviet.ma_cd = chude.ma_cd
                  WHERE baiviet.ma_tv='".$ma_tv."'"; 
        
        $ten_tv = "";
        $ma_tv = "";
        $noi_dung = "";
        $ten_cd = "";
        $ma_bv = "";
        
        $result = mysqli_query($conn, $query);
        if(mysqli_num_rows($result) > 0)
        {
            while($row = mysqli_fetch_object($result))
            {
                $ten_tv = $row->ten_tv;
                $ma_tv = $row->ma_tv;
                $noi_dung = $row->noi_dung;
                $ten_cd = $row->ten_cd;
                $ma_bv =$row->ma_bv;

            }
        }
        mysqli_free_result($result);

    ?>
    <form action="" method="POST">
    <table align="center">
        <tr>
            <th align="center" colspan="2">THÔNG TIN BÀI VIẾT</th>
        </tr>
        <tr>
            <td>Mã bài viết</td>
            <td>
                <input type="text" name="ma_tv_txt" value="<?php echo $ma_bv; ?>" disabled="disabled">
            </td>
        </tr>
        <tr>
            <td>Tiêu đề</td>
            <td>
                <input type="text" name="ten_tv_txt" value="<?php echo $ten_tv; ?>" required>
            </td>
        </tr>
        <tr>
            <td>Nội dung</td>
            <td>
                <textarea name="noi_dung" id="" cols="30" rows="10"><?php echo $noi_dung; ?></textarea>
            </td>
        </tr>
        <tr>
            <td>Tên chủ đề</td>
            <td>
                <input type="text" name="ten_cd_txt" value="<?php echo $ten_cd; ?>" required>
            </td>
        </tr>
        <tr>
            <td>Nội dung thảo luận</td>
            <td>
                <input type="text" name="noi_dung_tl" value="" required>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <input type="submit" name="updateBtn" value="Cập nhật" id="updateBtn">
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <a href="VietMinh_TuanDat.php">Quay về trang trước</a>
            </td>
        </tr>
    </table>
    </form>
    <?php
        if(isset($_POST["updateBtn"]))
        {
            $noi_dung_tl = $_REQUEST['noi_dung_tl'];
           
            $sql_update = "UPDATE thaoluan 
                            SET noi_dung_tl = '".$noi_dung_tl."'
                            WHERE ma_tv = '".$ma_tv."'' && $ma_bv = '".$ma_bv."'";
            if(mysqli_query($conn, $sql_update))
            {
                echo '<p align="center">CẬP NHẬT THÔNG TIN THÀNH CÔNG!!</p>';
                echo '<p align="center"><a href="VietMinh_TuanDat.php">Quay về</a></p>';
            }
            else
                echo '<p align="center">KHÔNG CẬP NHẬT ĐƯỢC!</p>';


        } 
    ?>
</body>
</html>
