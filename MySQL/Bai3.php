
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lưới phân trang</title>
	<style>
		a:link, a:visited:active {
			  
			  text-decoration: none;
			  color: (internal value);
		}
	</style>
</head>
<body>
	<?php
	// 1. Ket noi CSDL
	

	// 2. Chuan bi cau truy van & 3. Thuc thi cau truy van
	if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }
        $no_of_records_per_page = 5;
        $offset = ($pageno-1) * $no_of_records_per_page;

        require('connection.php');
        // Check connection
        if (mysqli_connect_errno()){
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            die();
        }

        $total_pages_sql = "SELECT COUNT(*) FROM sua";
        $result = mysqli_query($conn,$total_pages_sql);
        $total_rows = mysqli_fetch_array($result)[0];
        $total_pages = ceil($total_rows / $no_of_records_per_page);

        $sql = "SELECT s.Ten_sua, hs.Ten_hang_sua, ls.Ten_loai, s.Trong_luong, s.Don_gia
				FROM sua s 
				JOIN hang_sua hs ON s.Ma_hang_sua = hs.Ma_hang_sua
				JOIN loai_sua ls ON s.Ma_loai_sua = ls.Ma_loai_sua 
				LIMIT $offset, $no_of_records_per_page";
        $res_data = mysqli_query($conn,$sql);
		echo "<p align='center' style='font-weight: bold;'><font size='5' color='#eb4034'> THÔNG TIN HÃNG SỮA</font></P>";
		echo "<table align='center' width='1000' border='1' cellpadding='2' cellspacing='2' style='bordercollapse:collapse'>";
		echo "<tr style = 'color: #eb4034; background-color: #fab696;'>
			<th width='50'>STT</th>
			<th width='150'>TÊN SỮA</th>
			<th width='100'>HÃNG SỮA</th>
			<th width='100'>LOẠI SỮA</th>
			<th width='100'>TRỌNG LƯỢNG</th>
			<th width='100'>DƠN GIÁ</th>
		  </tr>";	 
	$stt=1;
	while($rows=mysqli_fetch_row($res_data))
	{   
		if ($stt % 2 == 0)
		{
			echo "<tr align='center' style='background-color: #fab696;'>";
			echo "<td>".$stt."</td>";
			echo "<td>$rows[0]</td>";
			echo "<td>$rows[1]</td>";
			echo "<td>$rows[2]</td>";
			echo "<td>$rows[3]</td>";
			echo "<td>$rows[4]</td>";
			echo "</tr>";
		}
		else {
			echo "<tr align='center' style='background-color: #f5e7c6;'>";
			echo "<td>".$stt."</td>";
			echo "<td>$rows[0]</td>";
			echo "<td>$rows[1]</td>";
			echo "<td>$rows[2]</td>";
			echo "<td>$rows[3]</td>";
			echo "<td>$rows[4]</td>";
			echo "</tr>";
		}
	$stt+=1;
	}//while
	
	
	// 5. Xoaa ket qua khoi vung nho va Dong ket noi
	//mysqli_free_result($result);
	mysqli_close($conn);
	?>
	<tr class="pagination" align="center">
        <td class="pagination" colspan="6"><a href="?pageno=1">
		        <span><a href="?pageno=1"><<</a></span>
		        <span class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
		            <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>"><</a>
		        </span>
		        <?php for($i = 1; $i < $total_pages + 1; $i++)
		        		echo "<a href='?pageno=".$i."'>".$i."</a>"."  ";

		         ?>
		        <span class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
		            <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">></a>
		        </span>
		        <span><a href="?pageno=<?php echo $total_pages; ?>">>></a></span>    
        </td>
        
    </tr>
    <?php echo"</table>";  ?>
</body>
</html>