<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chi tiết</title>
    <style>
        table{
            width: 50%;
        }
    </style>
</head>
<body>
    <table align="center" border="1" width="60%">
                <?php
                    require('connect.php');
                    if(isset($_GET['ma_sua']))
                    {
                        $ma_sua = $_GET['ma_sua'];
                        $sql = "SELECT Ma_sua, Ten_sua, Ten_hang_sua, Trong_luong, Don_gia, TP_Dinh_Duong, Loi_ich, Hinh
                                FROM sua AS s JOIN hang_sua AS hs ON s.Ma_hang_sua = hs.Ma_hang_sua 
                                            JOIN loai_sua AS ls ON s.Ma_loai_sua = ls.Ma_loai_sua
                                WHERE Ma_sua = '".$ma_sua."'";
                        $result = mysqli_query($conn, $sql);
                        if(mysqli_num_rows($result) > 0)
                        {
                            if($row = mysqli_fetch_object($result))
                            {
                            echo '<tr>';
                                echo '<th colspan="2" align="center" style="color: #f27d1d;background-color: #fcdfd2;">'.$row->Ten_sua.'-'.$row->Ten_hang_sua.'</th>';
                            echo '</tr>';

                            echo '<tr>';
                                echo '<td align="center" width="200px"> <img src="Hinh_sua/'.$row->Hinh.'" width="100px" height="100px"></td>';
                        
                                echo '<td>';                  
                                    echo '<b> Thành phần dinh dưỡng: </b></br>'.$row->TP_Dinh_Duong.'</br>';
                                    echo '<b>Lợi ích:</b> </br>'.$row->Loi_ich.'</br>';
                                    echo '<p width="100" align="right"><b>Trọng lượng: </b>'.$row->Trong_luong.' gr - <b>Đơn giá:</b> '.$row->Don_gia.' VNĐ</p>';
                                echo '</td>';

                            echo '</tr>';

                            echo '<tr align="right"><td>';
                                echo '<a href="Bai6.php">Quay về </a>';
                            echo '</td></tr>';
                            }
                        }
                    } 
                ?>
    </table>
</body>
</html>