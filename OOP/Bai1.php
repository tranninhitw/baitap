<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Quản lý nhân viên</title>
	<style>
        form {
            background-color: #ccd9cf;
            margin-top: 5%;
            left: 25%;
            min-width: 30%;
            position: absolute;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        h2{
            background-color: #3caf2e;
            color: white;
            margin: 0px;
            padding: 10px 0px;
            text-align: center;
        }
        tr{
            background-color:#f8f8a7 ;
        }
       
    </style>
</head>
<body>
	<?php 
		abstract class NhanVien{
			const lcb = 2000000;
			protected $hoten, $gioitinh, $ngayvao, $hsl, $socon; 
			public function getLCB()
			{
			    return self::lcb;
			}
		
			public function getHSL()
			{
			    return $this->hsl;
			}
			public function setHSL($hsl)
			{
			    $this->hsl = $hsl;
			    return $this;
			}
			public function getNVL()
			{
			    return $this->ngayvao;
			}
			public function setNVL($ngayvao)
			{
			    $this->ngayvao = $ngayvao;
			    return $this;
			}
			public function getHoTen()
			{
			    return $this->hoten;
			}
			
			public function setHoTen($hoten)
			{
			    $this->hoten = $hoten;
			    return $this;
			}
			
			public function getGioiTinh()
			{
			    return $this->gioitinh;
			}
			public function setGioiTinh($gioitinh)
			{
			    $this->gioitinh = $gioitinh;
			    return $this;
			}
			public function getSoCon()
			{
			    return $this->socon;
			}
			public function setSoCon($socon)
			{
			    $this->socon = $socon;
			    return $this;
			}
			abstract public function TinhTienLuong();
			abstract public function TinhTroCap();
			abstract public function TinhTienThuong();
			
		}
		class NhanVienVP extends NhanVien
		{
			const dm_vang = 5;
			const dg_phat = 200000;
			protected $songayvang;
			
			public function getSNV()
			{
			    return $this->songayvang;
			}
			public function setSNV($songayvang)
			{
			    $this->songayvang = $songayvang;
			    return $this;
			}
			public function TinhTienPhat()
			{
				if ($this->getSNV() > self::dm_vang)
					return $this->getSNV() * self::dg_phat;
				return 0;
			}
			public function TinhTroCap()
			{
				if ($this->getGioiTinh() == "Nữ")
					return 200000 * $this->getSoCon() * 1.5;
				else
					return 200000 * $this->getSoCon();
			}
			public function TinhTienThuong()
			{
				if (date("Y") - explode("/", $this->getNVL())[2] >= 1)
					return (date("Y") - explode("/", $this->getNVL())[2]) * 1000000;
				else
					return 0;
			}
			public function TinhTienLuong()
			{
				return $this->getLCB() * $this->getHSL() ;
			}
			
		}
		class NhanVienSX extends NhanVien
		{
			const dm_sanpham = 200;
			const dg_sanpham = 30000;
			protected $sosanpham;
			public function getSP()
			{
			    return $this->sosanpham;
			}
			public function setSP($sosanpham)
			{
			    $this->sosanpham = $sosanpham;
			    return $this;
			}
			public function TinhTienThuong()
			{
				if ($this->getSP() > self::dm_sanpham)
					return ($this->getSP() - self::dm_sanpham) * self::dg_sanpham * 0.03;
				else
					return 0;
			}
			public function TinhTroCap()
			{
				return $this->getSoCon() * 120000;
			}
			public function TinhTienLuong()
			{
				return ($this->getSP() * self::dg_sanpham);
			}
		}
		if(isset($_POST['hoten'])) $hoten = $_POST['hoten']; else $hoten = ' ';
		if(isset($_POST['gioitinh'])) $gioitinh = $_POST['gioitinh']; else $gioitinh = ' ';
		if(isset($_POST['ngayvaolam'])) $ngayvaolam = $_POST['ngayvaolam']; else $ngayvaolam = ' ';
		if(isset($_POST['hesoluong'])) $hesoluong = $_POST['hesoluong']; else $hesoluong = ' ';
		if(isset($_POST['socon'])) $socon = $_POST['socon']; else $socon = ' ';
		if(isset($_POST['ngayvang'])) $ngayvang = $_POST['ngayvang']; else $ngayvang = 0;
		if(isset($_POST['sanpham'])) $sanpham = $_POST['sanpham']; else $sanpham = 0;
		if(isset($_POST['submit'])) {
			if (isset($_POST['loainhanvien']))
				if ($_POST['loainhanvien'] == "Văn phòng")
				{
						$NhanVien = new NhanVienVP();
						$NhanVien->setHoTen($hoten);
						$NhanVien->setGioiTinh($gioitinh);
						$NhanVien->setNVL($ngayvaolam);
						$NhanVien->setHSL($hesoluong);
						$NhanVien->setSoCon($socon);
						$NhanVien->setSNV($ngayvang);
						$tienluong = $NhanVien->TinhTienLuong();
						$tientrocap = $NhanVien->TinhTroCap();
						$tienphat = $NhanVien->TinhTienPhat();
						$tienthuong = $NhanVien->TinhTienThuong();
						$tong = $tienluong + $tientrocap + $tienthuong - $tienphat;
						$sanpham = 0;
						$ngayvang = $_POST['ngayvang'];
				
				}
				else
				{

						$NhanVien = new NhanVienSX();
						$NhanVien->setHoTen($hoten);
						$NhanVien->setGioiTinh($gioitinh);
						$NhanVien->setNVL($ngayvaolam);
						$NhanVien->setHSL($hesoluong);
						$NhanVien->setSoCon($socon);
						$NhanVien->setSP($sanpham);
						$tienluong = $NhanVien->TinhTienLuong();
						$tientrocap = $NhanVien->TinhTroCap();
						$tienthuong = $NhanVien->TinhTienThuong();
						$tienphat = 0;
						$tong = $tienluong + $tientrocap + $tienthuong - $tienphat;
						$ngayvang = 0;
						$sanpham = $_POST['sanpham'];
				}
		}
		else
		{
			$tienluong = 0;
			$tientrocap = 0;
			$tienthuong = 0;
			$tienphat = 0;
			$tong = 0;
		}
		
	 ?>
	 
	 <form action="" method="post">
    <h2>QUẢN LÝ NHÂN VIÊN</h2>
    <table align="left">
        <tr>
            <td>Họ và tên: </td>
            <td style="width: 300px"><input type="text" name="hoten" style="width: 90%" value="<?php echo $hoten;?>" required></td>
            <td>Số con: </td>
            <td><input type="number" name="socon" style="width: 60%" value="<?php echo $socon?>" required min="0"></td>
        </tr>
        <tr>
            <td>Ngày sinh: </td>
            <td><input type="text" name="ngaysinh"  required></td>
            <td>Ngày vào làm: </td>
            <td><input type="text" name="ngayvaolam" value="<?php echo $ngayvaolam;?>"required></td>
        </tr>
        <tr>
            <td>Giới tính: </td>
            <td>
                <input type="radio" name="gioitinh" value="Nam" checked>Nam&nbsp;
                <input type="radio" name="gioitinh" value="Nữ">Nữ
            </td>
            <td>Hệ số lương: </td>
            <td><input type="number" name="hesoluong" style="width: 60%" step="any" value="<?php echo $hesoluong;?>" required></td>
        </tr>
        <tr>
            <td>Loại nhân viên: </td>
            <td><input type="radio" name="loainhanvien" value="Văn phòng" checked>Văn phòng</td>
            <td colspan="2"><input type="radio" name="loainhanvien" value="Sản xuất">Sản xuất</td>
        </tr>
        <tr>
            <td></td>
            <td>Số ngày vắng: <input type="number" style="width: 35%" min="0" name="ngayvang" value="<?php echo $ngayvang;?>"></td>
            <td colspan="2">Số sản phẩm: <input type="number" style="width: 35%" min="0" name="sanpham" value="<?php echo $sanpham;?>"></td>
        </tr>
        <tr style="background-color: #ccd9cf" align="center">
            <td colspan="4"><button type="submit" style="border-style: solid; border-width: 0.5px; background-color: #31bbe8; border-radius: 4px; color: white; height: 25px;" name="submit">Tính lương</button></td>
        </tr>
        <tr align="center">
            <td>Tiền lương: </td>
            <td class="element"><input type="text"  disabled="disabled" value="<?php echo $tienluong; ?>"></td>
            <td>Trợ cấp: </td>
            <td class="element" ><input type="text"  disabled="disabled" value="<?php echo $tientrocap?>"></td>
        </tr>
        <tr align="center">
            <td>Tiền thưởng: </td>
            <td><input type="text" disabled="disabled" value="<?php echo $tienthuong?>"></td>
            <td>tiền phạt: </td>
            <td><input type="text" disabled="disabled" value="<?php echo $tienphat?>"></td>
        </tr>
        <tr align="center">
            <td colspan="4">Thực lĩnh <input type="text" disabled="disabled" value="<?php echo $tong?>"></td>
        </tr>
    </table>
</form>
</body>
</html>