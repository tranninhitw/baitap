<?php
	 class PhanSo{
		private $ts, $ms;

		public function setTS($ts)
		{
		    $this->ts = $ts;
		    return $this;
		}

		public function getTS()
		{
		    return $this->ts;
		}

		public function setMS($ms)
		{
		    $this->ms = $ms;
		    return $this;
		}

		public function getMS()
		{
		    return $this->ms;
		}
		public function UCLN($x,$y){
			if ($x < 0)
	    	{
				$x *= -1;
	    	}
    		if ($y < 0)
    		{
				$y *= -1;
    		}
        	if($x == 0)
            	return 0;
    		while ($x != $y)
        	{
	   			if ($x > $y)
    				$x = $x - $y;
        		else
	       			$y = $y - $x;
        	}
	  		return $x;
		}
		public function InPS()
		{
			return $this->getTS()."/".$this->getMS();
		}
		public function RutGon(){
			$tam1 = $this->getTS();
        	$tam2 = $this->getMS();
        	$uc = 0;
        	$uc = $this->UCLN($tam1,$tam2);
        	if($uc != 0){
        		$this->ts = $this->ts/$uc;
        		$this->ms = $this->ms/$uc;
        	}
        	else 
        	{
        		$this->ts = $this->ts;
        		$this->ms = $this->ms;
			}
		}
		public function Cong(PhanSo $ps){
			if ($this->getMS() != $ps->getMS()){
				$tstam = ($this->getTS() * $ps->getMS()) + ($this->getMS() * $ps->getTS());
				$this->setTS($tstam);
				$mstam = ($this->getMS() * $ps->getMS());
				$this->setMS($mstam);
				$this->RutGon();
			}
			else{
				$tstam = $this->getTS() + $ps->getTS();
				$this->setTS($tstam);
				$mstam = $ps->ms;
				$this->setMS($mstam);
				$this->RutGon();
			}
		}
		public function Tru(PhanSo $ps){
			if ($this->getMS() != $ps->getMS()){
				$tstam = ($this->getTS() * $ps->getMS()) - ($this->getMS() * $ps->getTS());
				$this->setTS($tstam);
				$mstam = ($this->getMS() * $ps->getMS());
				$this->setMS($mstam);
				$this->RutGon();
			}
			else{
				$tstam = $this->getTS() - $ps->getTS();
				$this->setTS($tstam);
				$mstam = $ps->ms;
				$this->setMS($mstam);
				$this->RutGon();
			}
		}
		public function Nhan(PhanSo $ps){
			
				$tstam = $this->getTS() * $ps->getTS();
				$this->setTS($tstam);
				$mstam = $this->getMS() * $ps->getMS();
				$this->setMS($mstam);
				$this->RutGon();
			
		}
		public function Chia(PhanSo $ps){
				$tstam = $this->getTS() * $ps->getMS();
				$this->setTS($tstam);
				$mstam = $this->getMS() * $ps->getTS();
				$this->setMS($mstam);
				$this->RutGon();
		}
	}
	if(isset($_POST['ts1'])) $ts1 = $_POST['ts1']; else $ts1 = 0;
	if(isset($_POST['ts2'])) $ts2 = $_POST['ts2']; else $ts2 = 0;
	if(isset($_POST['ms1'])) $ms1 = $_POST['ms1']; else $ms1 = 0;
	if(isset($_POST['ms2'])) $ms2 = $_POST['ms2']; else $ms2 = 0;
	if(isset($_POST['pt'])) $pt = $_POST['pt']; else $pt = '+';

	$alert = ' ';
	$dau = ' ';
	$bool = 0;
	if(isset($_POST["submit"])){
		

    	$phanso1 = new PhanSo();
	    $phanso1->setTS($ts1);
	    $phanso1->setMS($ms1);
	    //$phanso1->RutGon();

	    $phanso2 = new PhanSo();
	    $phanso2->setTS($ts2);
	    $phanso2->setMS($ms2);
	    //$phanso2->RutGon();
	    $kq = new PhanSo();
	    $kq->setTS($ts1);
	    $kq->setMS($ms1);
	    if ($ms1 * $ms2 == 0)
	    	$alert = "Xin vui lòng nhập lại mẫu số!!!";
	    else
	    switch ($pt) {
	    	case '+':
	    			$kq->Cong($phanso2);
	    			$kq->RutGon();
	    			$dau = "+";
	    			$bool = 0;
	    	
	    		break;
	    	case '-':
	    			$kq->Tru($phanso2);
	    			$kq->RutGon();
	    			$dau = "-";
	    			$bool = 0;
	    	
	    		break;
	    	case '*':
	    			$kq->Nhan($phanso2);
	    			$kq->RutGon();
	    			$dau = "*";
	    			$bool = 0;
	    	
	    		break;
	    	case '/':
	    			if($phanso2->getTS() == 0){
	    				$alert = 'Xin vui lòng nhập lại tử số thứ 2 để thực hiện phép chia!!!';
	    				$bool = 1;
	    			}
	    			else{
		    			$kq->Chia($phanso2);
		    			$kq->RutGon();
		    			$dau = "/";
	    			}
	    		break;
	    	
	    	default:
	    		// code...
	    		break;
	    }
	} else $kq = new PhanSo();
  ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Phân số</title>
	<style type="text/css">
		.input{
			width: 50%;
		}
	</style>
</head>
<body>
	<form action="" method="post">
	<table bgcolor="#b1dbdc" align="center" style="width: 500px;" border="0">
		<tr bgcolor="#2d9598" align="center">
			<td colspan="3"><h2 style="color: white;">Các phép tính trên phân số</h2></td>
		</tr>
		<tr>
			<td style="width: 35%; text-align: right;">
				Nhập phân số thứ 1:
			</td>
			<td align="center">
				Tử số: <input class="input" type="number" name="ts1" value="<?php echo $ts1; ?>"> 
			</td>
			<td>
				Mẩu số:<input class="input" type="number" name="ms1" value="<?php echo $ms1; ?>">
			</td>
		</tr>
		<tr>
			<td style="text-align: right;">
				Nhập phân số thứ 2:
			</td>
			<td align="center">
				Tử số: <input class="input" type="number" name="ts2" value="<?php echo $ts2; ?>"> 
			</td>
			<td>
				Mẩu số:<input class="input" type="number" name="ms2" value="<?php echo $ms2; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center">
			<fieldset style="width: 250px; margin-bottom: 20px;">
		 		<legend>Chọn phép tính</legend>
		 		<span>
		 			<input type="radio" name="pt" value="+" checked="checked">Cộng
		 			<input type="radio" name="pt" value="-">Trừ
		 			<input type="radio" name="pt" value="*">Nhân
		 			<input type="radio" name="pt" value="/">Chia
		 		</span>
	 		</fieldset>
			</td>
		</tr>	 	 
	 	<tr>
	 		<td colspan="3" style="text-align: center; ">
	 			<input type="submit" style="border-style: outset; background-color: lightyellow;" name="submit" value="Kết quả">
	 		</td>
	 	</tr>
	 	<tr>
	 		<td colspan="3" align="center" style="color: red;font-weight: bold;">
	 			<?php 	if ($ms1 == 0 || $ms2 == 0 || $bool == 1)
	 				 echo $alert;
	 			else if (($ts1 * $ts2 == 0 ) && $pt != '+' && $pt != '-' && $bool == 0)
	 				echo $phanso1->InPS()." ".$dau." ".$phanso2->InPS().' = 0';
	 			else if ($kq->getTS() == 1 && $kq->getMS() == 1)
	 				echo $phanso1->InPS()." ".$dau." ".$phanso2->InPS().' = 1';
	 			else
	 				echo $phanso1->InPS()." ".$dau." ".$phanso2->InPS().' = '.$kq->InPS();
	 	 ?>
	 		</td>
	 	</tr>

	 	
	 	
	 	
	 </table>
	 </form>
</body>
</html>