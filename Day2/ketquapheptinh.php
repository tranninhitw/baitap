 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>	Kết quả phép tính</title>
 </head>
 <body>
 	<?php 
 		$number1 = ($_POST['num1']);
 		$number2 = ($_POST['num2']);
 		$calVal = $_POST['cal'];
 		if ( $calVal == "Cong"){
 			$result = ($number1 + $number2);
 			$calVal = "Cộng";
 		} else if ( $calVal == "Tru"){
 			$result = ($number1 - $number2);
 			$calVal = "Trừ";
 		} else if ( $calVal == "Nhan"){
 			$result = ($number1 * $number2);
 			$calVal = "Nhân";
 		} else if ( $calVal == "Chia"){
 			if ( $number2 != 0){
 				$result = ($number1 / $number2);
 			} else {
 				echo '<script language="javascript">';
				echo 'alert("Không thể chia cho 0 !!!")';
				echo '</script>';
				header('Location: http://localhost/BaiTap/Day2/tinh2so.php');
 			}
 		}
  	?>
  	<table align="center"  border="0">
	 		<tr>
	 			<th colspan="2" align="center"><h2><font color="red">
	 				PHÉP TÍNH TRÊN 2 SỐ
	 			</font></h2></th>
	 		</tr>
	 		<tr>
	 			<td>Phép tính: </td>
	 			<td>
	 				<?php echo $calVal; ?>
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td>Số thứ nhất: </td>
	 			<td><input style="width: 100%;"  type="text" name="num1" value="<?php echo $number1; ?>"></td>
	 		
	 		</tr>
	 		<tr>
	 			<td>Số thứ hai: </td>
	 			<td><input style="width: 100%;" type="text" name="num2" value="<?php echo $number2; ?>"></td>
	 		</tr>
	 		<tr>
	 			<td>Kết quả: </td>
	 			<td><input style="width: 100%;" type="text" name="result" value="<?php echo $result; ?>"></td>
	 		</tr>
	 		<tr>
	 			<td colspan="2" align="center">
	 				<a href="javascript:window.history.back(-1);">Quay lại trang trước ...</a>
	 			</td>
	 		</tr>
	 	</table>
 </body>
 </html>