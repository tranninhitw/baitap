<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
	<head>
		<title>Welcome</title>
		<link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
		<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
		<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/jquery.min.js"></script>
		 <!-- Custom Theme files -->
		<link href="css/theme-style.css" rel='stylesheet' type='text/css' />
   		 <script src="js/jquery.easing.min.js"></script>
   		 <!-- Custom Theme files -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		</script>
		<!----webfonts---->
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,300,500,700,800,900,600,200' rel='stylesheet' type='text/css'>
		<!----//webfonts---->
		<!----requred-js-files---->
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		    <script src="js/html5shiv.js"></script>
		    <script src="js/respond.min.js"></script>
		<![endif]-->
		<!----//requred-js-files---->
		<script type="text/javascript" 	src="js/jquery.smint.js"></script>
		<script type="text/javascript">
			$(document).ready( function() {
			    $('.subMenu').smint({
			    	'scrollSpeed' : 1000
			    });
			});
		</script>
	</head>
	<body onload="setTimeout(function() { window.scrollTo(0, 1) }, 100);">
		<?php
			$temp = 0;
			$url1 = '/Array/';
			if(isset($_GET['linka'])) {
				$link1 = $_GET['linka'];
				$url1 = $url1.$link1;
				$temp = 1;
			}
			else
				$url1 = $url1."Bai1.php";
			$url = '/BaiTap/MySQL/';
			if(isset($_GET['link'])) {
				$link = $_GET['link'];
				$url = $url.$link;
				$temp = 0;
			}
			else
				$url = $url."Bai1.php";
			
			
		 ?>
		<!----start-container----->
		<div class="head-bg sTop">
			<div class="conatiner">
				<div class="col-lg-12 header-note">
					<h2>NGUYEN TUAN DAT</h2>
					<h3>MSSV: 58131281</h3>
					<h3>58TH1</h3>
				</div>
			</div>
		</div>
		<!----//End-container----->
		<!----start-top-nav---->
		<nav class="subMenu navbar-custom navbar-scroll-top" role="navigation">
	        <div class="container">
	            <div class="navbar-header page-scroll">
	                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
	                    <img src="images/nav-icon.png" title="drop-down-menu" /> 
	                </button>
	            </div>
	            <!-- Collect the nav links, forms, and other content for toggling -->
	            <div class="collapse navbar-collapse navbar-left navbar-main-collapse">
	                <ul class="nav navbar-nav">
	                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
	                    <li class="page-scroll">
	                        <a id="s3" class="subNavBtn" href="http://localhost/">Home</a>
	                    </li>
	                    <li class="page-scroll">
	                        <a id="s1" class="subNavBtn" href="/BaiTap/Form.php">Form</a>
	                    </li>
	        
	                    <li class="page-scroll">
	                        <a id="s3" class="subNavBtn" href="/BaiTap/Array.php">Array</a>
	                    </li>
	                     <li class="page-scroll">
	                        <a id="s3" class="subNavBtn" href="/BaiTap/OOP.php">OOP</a>
	                    </li>
	                    <li class="page-scroll">
	                        <a id="sTop" class="subNavBtn" href="#">MySQL</a>
	                    </li>
	                </ul>
	            </div>
	            <!-- /.navbar-collapse -->
	             	<a  id="s4" class="right-msg subNavBtn msg-icon"href="#"><span> </span></a>
	                <div class="clearfix"> </div>
	        </div>
	        <!-- /.container -->
   	  </nav>
		<!----//End-top-nav---->
		<!---- start-top-grids---->
		<!---- //End-top-grids---->
		<!----start-about---->
		<div class="about">
			<div class="container">
				<div class="col-md-6 divice"  style="width: 20%;">
					<h3>OOP</h3>
					<a href="MySQL.php?link=Bai1.php"><p>1. Hiển thị lưới</p></a>
					<a href="MYSQL.php?link=Bai2.php"><p>2. Lưới định dạng - tùy biến</p></a>
					<a href="MYSQL.php?link=Bai3.php"><p>3. Lưới phân trang</p></a>
					<a href="MYSQL.php?link=Bai4.php"><p>4. List đơn giản</p></a>
					<a href="MYSQL.php?link=Bai5.php"><p>5. List dạng cột</p></a>
					<a href="MYSQL.php?link=Bai6.php"><p>6. List dạng cột có link</p></a>
					<a href="MYSQL.php?link=Bai7.php"><p>7. List chi tiết có phân trang</p></a>
					<a href="MYSQL.php?link=Bai8.php"><p>8. Tìm kiếm đơn giản</p></a>
					<a href="MYSQL.php?link=Bai9.php"><p>9. Tìm kiếm nâng cao</p></a>
					<a href="MYSQL.php?link=Bai10.php"><p>10. Thêm mới</p></a>
					<a href="MYSQL.php?link=Bai11.php"><p>11. Xóa - Sửa</p></a>
					
				</div>
				<div class="col-md-6 divice-info"  style="width: 80%;">
					<iframe width="100%" height="400px" frameborder="0" src="<?php if ($temp == 1) echo $url1; else echo $url; ?>"></iframe>
				
				</div>
			</div>
		</div>
		<!----//End-about---->
		<!----start-portfolio---->
		
		<!----//End-contact---->
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>

