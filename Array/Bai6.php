<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sắp xếp mảng</title>
</head>
<body>
	<?php
		function print_a($a){
			$lass = count($a) - 1;
			for ($i = 0; $i < count($a)-1; $i++){
				echo $a[$i];
				echo ", ";
			}
			echo $a[$lass];
		}
		function replace_x($x,$new ,$a){
			$index = 0;
			for ($i = 0; $i < count($a); $i++){
				if ($x == $a[$i])
					$a[$i] = $new;
			}
			return $a;
		}
		function inc_sort($a){
			sort($a);
			return $a;
		}
		function de_sort($a){
			rsort($a);
			return $a;
		}
	 	if(isset($_POST['str'])) {
	 		$str = $_POST['str']; 

	 	}
	 	else $str = ' ';
	 	if(isset($_POST['oldnum'])) {
	 		$oldnum = $_POST['oldnum']; 
	 	}
	 	else $oldnum = ' ';
	 	if(isset($_POST['newnum'])) {
	 		$newnum = $_POST['newnum']; 
	 	}
	 	else $newnum = ' ';
		if(isset($_POST['tinh'])) {
			$array = explode(',', $str);
			$in_array = inc_sort($array);
			$de_array = de_sort($array);
			
		}
		else {$array = ' '; $in_array = ' '; $de_array = ' ';}
		if(isset($_POST['reset'])) {$array = ' ';}
		
	   ?>
		
	 <form action="" method="post">
	 	<table align="center" bgcolor="#b1dbdc"  border="0">
	 		<tr bgcolor="#2d9598">

	 			<th colspan="2" align="center"><h2><font color="white">
	 				SẮP XẾP MẢNG
	 			</font></h2></th>
	 		</tr>
	 		<tr>
	 			<td>Nhập các phần tử: </td>
	 			<td>
	 				<input style="width: 90%;" onkeypress="return (event.charCode !=8  || (event.charCode >= 43 && event.charCode <= 57))" type="text" name="str" value="<?php echo  $str?>">
	 			</td>
	 		
	 		</tr>
	 		
	 		<tr>
	 			<td></td>
	 			<td>
	 				<input type="submit" value="Sắp xếp tăng/giảm" name="tinh">
	 			</td>
	 		</tr>
	 			<tr>
	 			<td>Sau khi sắp xếp: </td>
	 			<td>
		 			
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td>Tăng dần: </td>
	 			<td>
		 			<input style="width: 90%;"  disabled="disabled" type="text" name="in_array" value="<?php echo print_a($in_array); ?>">
	 			</td>
	 			
	 		</tr>
	 		<tr>
	 			<td>Giảm dần: </td>
	 			<td>
		 			<input style="width: 90%;"  disabled="disabled" type="text" name="de_array" value="<?php echo print_a($de_array); ?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<th colspan="2" align="center">
	 				(<font color="red">Ghi chú: </font>
	 				các phần tử trong mảng sẽ cách nhau bằng dấu ",")
	 				
	 			</th>
	 		</tr>
	 		
	 	</table>
	 </form>
</body>
</html>