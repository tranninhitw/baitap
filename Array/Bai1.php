<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Xử lý mảng</title>
	<style type="text/css">
		.text{
			width: 40%;
			font-weight: bold;
			text-align: right;

		}
		.inset{
			border-style: inset;
			background-color: lightyellow;
			color: red;
		}
		.button{
			border-style: outset;
			background-color: lightyellow;
		}
	</style>
</head>
<body>
	<?php
		function print_a($a){
			$lass = count($a) - 1;
			for ($i = 0; $i < count($a)-1; $i++){
				echo $a[$i];
				echo ", ";
			}
			echo $a[$lass];
		}
		function sort_a($a){
			sort($a);
			return $a;
		}
	 	if(isset($_POST['num'])) {
	 		$num = $_POST['num']; 
	 		
	 	}
	 	else $num = 0;
		if(isset($_POST['tinh'])) {
			for($i = 0; $i < $num; $i++){
				$a[$i] = rand(0,20);
			}
	 		$b = sort_a($a);
		}
		else {$num = 0 ;$a = ' '; $b = ' ';}
		if(isset($_POST['reset'])) {$num = 0; $a = ' '; $b = ' ';}
		function SC($num)  
		{  
			if($num % 2 == 0)  
				{  
				  return 1;  
				}  
		  	return 0;  
		}
		function lower100($num)  
		{  
			if($num < 100)  
				{  
				  return 1;  
				}  
		  	return 0;  
		}
		function lower0($num)  
		{  
			if($num < 0)  
				{  
				  return 1;  
				}  
		  	return 0;  
		}
		function zero($num)  
		{  
			if($num == 0)  
				{  
				  return 1;  
				}  
		  	return 0;   
		}
	   ?>
		
	 <form action="" method="post">
	 	<table bgcolor="#b1dbdc" style="width: 500px" align="center"  border="0">
	 		<tr bgcolor="#2d9598" >
	 			<th colspan="2" align="center"><h2><font color="white">
	 				Xử Lý Mảng
	 			</font></h2></th>
	 		</tr>
	 		<tr>
	 			<td class="text"><font color="blue">Nhập n: </font></td>
	 			<td>
	 				<input style="width: 80%;" class="inset" step="1" onkeypress="return (event.charCode !=8  || (event.charCode >= 47 && event.charCode <= 57))" type="number" name="num"  value="<?php echo $num; ?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td class="text">Các phần tử trong mảng: </td>
	 			<td>
	 				<input style="width: 80%;" class="inset" disabled="disabled" onkeypress="return (event.charCode !=8  || (event.charCode >= 43 && event.charCode <= 57))" type="text" name="a" value="<?php echo print_a($a); ?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td class="text">Số phần tử chẵn: </td>
	 			<td><?php
	 				$count = 0;
	 				
	 				for($i = 0; $i < $num; $i++){
	 					if (SC($a[$i])){
		 					$count ++;
		 					
	 					}
		 				
		 			}
		 			echo $count;
	 			 ?></td>
	 		</tr>
	 		<tr>
	 			<td class="text">Số phần bé hơn 100: </td>
	 			<td><?php
	 				$count = 0;
	 				for($i = 0; $i < $num; $i++){
	 					if (lower100($a[$i])){
		 					$count ++;
	 					}
		 				
		 			}
		 			echo $count;
	 			 ?></td>
	 		</tr>
	 		<tr>
	 			<td class="text">Tổng các phần tử âm: </td>
	 			<td><?php
	 				$sum = 0;
	 				for($i = 0; $i < $num; $i++){
	 					if (lower0($a[$i])){
		 					$sum += $a[$i];
	 					}
		 				
		 			}
		 			echo $sum;
	 			 ?></td>
	 		</tr>
	 		<tr>
	 			<td class="text">Vị trí phần tử có giá trị 0: </td>
	 			<td><?php
	 				for($i = 0; $i < $num; $i++){
	 					if (zero($a[$i])){
		 					echo $i + 1;
		 					echo ", ";
	 					}
		 				
		 			}
	 			 ?></td>
	 		</tr>
	 		<tr>
	 			<td class="text">Mảng sau khi sắp xếp: </td>
	 			<td><input style="width: 80%;" class="inset" disabled="disabled"  onkeypress="return (event.charCode !=8  || (event.charCode >= 43 && event.charCode <= 57))" type="text" name="a" value="<?php echo print_a($b); ?>"></td>
	 		</tr>
	 		<tr>
	 			<td colspan="2" align="center">
	 				<input class="button" type="submit" value="Xử lý" name="tinh">
	 				<input class="button" type="submit" value="Reset" name="reset">
	 			</td>
	 		</tr>
	 	</table>
	 </form>
</body>
</html>