<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Tìm kiếm</title>
</head>
<body>
	<?php
		function print_a($a){
			$lass = count($a) - 1;
			for ($i = 0; $i < count($a)-1; $i++){
				echo $a[$i];
				echo ", ";
			}
			echo $a[$lass];
		}
		function search_x($x, $a){
			$index = 0;
			for ($i = 0; $i < count($a); $i++){
				if ($x == $a[$i])
					return $index = $i + 1;
			}
			return $index = -1;
		}
	 	if(isset($_POST['str'])) {
	 		$str = $_POST['str']; 

	 	}
	 	else $str = ' ';
	 	if(isset($_POST['num'])) {
	 		$num = $_POST['num']; 
	 	}
	 	else $num = ' ';
		if(isset($_POST['tinh'])) {
			$array = explode(',', $str);
			$index = search_x($num,$array);
			if ( $index != -1)
				$alert = "Tìm thấy ".$num." tại vị trí thứ ".$index." của mảng";
			else
				$alert = "Không tìm thấy ".$num." trong mảng";
		}
		else{ $array = ' '; $alert = ' ';}
		if(isset($_POST['reset'])) {$array = ' ';}
		
	   ?>
		
	 <form action="" method="post">
	 	<table  bgcolor="#b1dbdc" align="center" style="width: 500px;" border="0">
	 		<tr bgcolor="#2d9598">
	 			<th colspan="2" align="center"><h2><font color="white">
	 				TÌM KIẾM
	 			</font></h2></th>
	 		</tr>
	 		<tr>
	 			<td  style="width: 30%;">Nhập dãy số: </td>
	 			<td>
	 				<input style="width: 90%;" onkeypress="return (event.charCode !=8  || (event.charCode >= 43 && event.charCode <= 57))" type="text" name="str" value="<?php echo  $str?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td>Nhập số cần tìm: </td>
	 			<td>
	 				<input style="width: 90%;" onkeypress="return (event.charCode !=8  || (event.charCode >= 43 && event.charCode <= 57))" type="text" name="num" value="<?php echo  $num?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td></td>
	 			<td>
	 				<input type="submit" style="border-style: outset; background-color: lightyellow;" value="Tìm kiếm" name="tinh">
	 			</td>
	 		</tr>
	 		<tr>
	 			<td>Mảng: </td>
	 			<td>
		 			<input style="width: 90%; background-color: lightyellow;"  disabled="disabled" type="text" name="array" value="<?php echo print_a($array); ?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td>Kết quả tìm kiếm : </td>
	 			<td>
		 			<input style="width: 90%; background-color: lightyellow;"  disabled="disabled" type="text" name="alert" value="<?php echo $alert; ?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<th colspan="2" align="center">
	 				<font color="red">
	 				(Các phần tử trong mảng sẽ cách nhau bằng dấu ",")
	 				</font>
	 			</th>
	 		</tr>
	 		
	 	</table>
	 </form>
</body>
</html>