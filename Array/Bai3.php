<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Phát sinh mảng và tính toán</title>
</head>
<body>
	<?php
		function print_a($a){
			$lass = count($a) - 1;
			for ($i = 0; $i < count($a)-1; $i++){
				echo $a[$i];
				echo ", ";
			}
			echo $a[$lass];
		}
		function create_a($n){
			$a = array($n);
			for ($i = 0; $i < $n; $i++)
				$a[$i] = rand (0, 20);
			return $a;
		}
		function GTLN($a){
			rsort($a);
			return $a[0];
		}
		function GTNN($a){
			sort($a);
			return $a[0];
		}
		function Sum($a){
			$sum = 0;
			for ($i = 0; $i < count($a); $i++)
				$sum += $a[$i];
			return $sum;
		}
	 	if(isset($_POST['num'])) {
	 		$num = $_POST['num']; 

	 	}
	 	else $num = ' ';
		if(isset($_POST['tinh'])) {
			$a = create_a($num);
			//rsort($a);
			$max = GTLN($a);
			$min = GTNN($a);
			$sum = Sum($a);
		}
		else
		{
			$a = ' ';
			$min = ' ';
			$max = ' ';
			$sum = ' ';
		}
		if(isset($_POST['reset'])) {$array = ' ';}
		
	   ?>
		
	 <form action="" method="post">
	 	<table bgcolor="#b1dbdc" style="width: 500px" align="center"  border="0">
	 		<tr bgcolor="#2d9598">
	 			<th colspan="2" align="center"><h2><font color="white">
	 				PHÁT SINH MẢNG VÀ TÍNH TOÁN
	 			</font></h2></th>
	 		</tr>
	 		<tr>
	 			<td style="width: 40%;">Nhập số phần tử: </td>
	 			<td>
	 				<input style="width: 90%;"  class="num" onkeypress="return (event.charCode !=8  || (event.charCode >= 43 && event.charCode <= 57))" type="text" name="num" value="<?php echo $num; ?>">
	 				<font color="red">(*)</font>
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td></td>
	 			<td>
	 				<input type="submit" style="border-style: outset; background-color: lightyellow;" value="Phát sinh và tính toán" name="tinh">
	 			</td>
	 		</tr>
	 		<tr>
	 			<td>Mảng: </td>
	 			<td>
		 			<input style="width: 90%; background-color: lightyellow;"  disabled="disabled" type="text" name="a" value="<?php echo print_a($a); ?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td>GTLN (MAX) trong mảng: </td>
	 			<td>
		 			<input style="width: 90%; background-color: lightyellow;"  disabled="disabled" type="text" name="max" value="<?php echo $max; ?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td>GTNN (MIN) trong mảng: </td>
	 			<td>
		 			<input style="width: 90%; background-color: lightyellow;"  disabled="disabled" type="text" name="min" value="<?php echo $min; ?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td>Tổng mảng: </td>
	 			<td>
		 			<input style="width: 30%; background-color: lightyellow;"  disabled="disabled" type="text" name="sum" value="<?php echo $sum; ?>">
	 			</td>
	 		
	 		</tr>
	 		<tr>
	 			<td colspan="2" align="center">
	 				<p>(<font color="red">Ghi chú: </font>Các phần tử trong mảng có giá trị từ 0 đến 20)</p>
	 			</td>
	 		</tr>
	 		
	 		
	 	</table>
	 </form>
</body>
</html>