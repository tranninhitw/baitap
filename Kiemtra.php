<?php
session_start();
$Hoa_stock = array(array(5,'Hoa Cúc'));
if(isset($_SESSION["Hoa"]))
    $Hoa = $_SESSION["Hoa"];
else
    $Hoa = $Hoa_stock;
function Xuat($arr){
    for ($i = 0; $i < count($arr); $i++)
        for($j = 0; $j < 1; $j++){
            echo "   ".$arr[$i][$j]."    | ".$arr[$i][$j+1];
        echo "\n";
        }
}
function Kiemtra($arr,$x){
    $dem = 0;
    for ($i = 0; $i < count($arr); $i++)
        for($j = 0; $j < 1; $j++)
            if($arr[$i][$j+1] == $x)
                $dem++;
    return $dem;
}
function AddItem($arr, $sl, $name){
    array_push($arr, array($sl,$name));
    return $arr;
}
$name = "";
$number = 0;
$alert = " ";
if(isset($_POST["submit"])){
    $name = $_POST["name"];
    $sl = $_POST["sl"];
    $dem = Kiemtra($Hoa,$name);
    if ($sl > 0 && $dem == 0){
        $Hoa = AddItem($Hoa,$sl,$name);
        $alert = " ";
    }
    else
        $alert = "Đã có hoa này rồi !!!";
    $_SESSION["Hoa"] = $Hoa;
}
?>
<head>
    <title>Giỏ hoa</title>
    <style>
        form{
            background-color: #ccd9cf;
            text-align: center;
            margin-top: 5%;
            left: 35%;
            position: absolute;
        }
        h2{
            background-color: #2d9498;
            color: white;
            margin: 0px;
            padding: 10px 0px;
        }
        textarea{
            margin: 5px;
        }
    </style>
</head>
<body>
<form action="" method="post">
    <h2>GIỎ HOA</h2>
    <table align="center">
        <tr>
            <td>Nhập tên loại hoa: </td>
            <td><input type="text" name="name" required></td>
        </tr>
        <tr>
            <td>Nhập số lượng: </td>
            <td><input type="number" name="sl" min="1" required value="1"></td>
        </tr>
        <tr>
            <td colspan="2" align="center"><font color="red"><?php echo $alert; ?></font></td>
            
        </tr>
    </table>
    <div>
        <textarea cols="50" rows="8" disabled="disabled">Số lượng| Tên Hoa<?php echo "\n";Xuat($Hoa);?></textarea>
    </div>
    <div>
        <button type="submit" style="border-style: outset; background-color: lightyellow; margin-bottom: 10px;" name="submit">Thêm Hoa</button>&nbsp;
    </div>
</form>
</body>